const path = require("path");
const fs = require("fs-extra");
const chalk = require("chalk");
const sleep = require("promise.delay");
const chai = require("chai");
const should = chai.should();
const CloudMachineTest = require("@rweda/cloud-machine-testing");
const GCETestMachine = require("@rweda/cloud-machine-testing/GCETestMachine");
const FormatTestOutput = require("@rweda/cloud-machine-testing/FormatTestOutput");

class RunnerConfigTest extends CloudMachineTest {

  constructor() {
    super();
    this.machine = new GCETestMachine();
    this.out = new FormatTestOutput();
  }

  prepareEnvironment() {
    return super
      .prepareEnvironment()
      .then(() => this.cleanOldLogs());
  }

  setup() {
    return super
      .setup()
      .then(() => this.waitForCloudInit())
      .then(() => this.fetchCloudInitLog());
  }

  /**
   * Runs automated tests on the remote server to ensure that `cloud-init` properly setup the machine.
   * @return {Promise} resolves when tests have finished.
  */
  automatedTests() {
    let _services = {};
    let _serviceAll = [];
    this.out.on("error", (err) => { ++this.exitCode; });
    for (const service of ["gitlab-runner", "stackdriver-gitlab-ci-relay"]) {
      const run = this.machine.ensureServiceStarted(service, 30 * 1000);
      _services[service] = run;
      _serviceAll.push(run);
    }
    return this.out
      .section("Start Services", () => {
        let _services = {};
        let _serviceAll = [];
        for (const service of ["gitlab-runner", "stackdriver-gitlab-ci-relay"]) {
          const run = this.machine.ensureServiceStarted(service, 30 * 1000);
          _services[service] = run;
          _serviceAll.push(run);
        }
        return Promise
          .all(_serviceAll)
          .then(() => {
            let its = [];
            for (const service in _services) {
              if(!_services.hasOwnProperty(service)) { continue; }
              its.push(this.out.it(`${service} should be running`, () => {
                return _services[service];
              }));
            }
            return its;
          });
      })
      .catch(err => true)
      .then(() => this.out.section("/etc/gitlab-runner/config.toml", () => {
        const prefix = "Autoscaling Compute Engine";
        const runnerID = `${prefix} ${this.machine._image}`;
        let runnerConfig;
        return this.out.it("fetches config file", () => {
            return this.machine
              .ssh("sudo cat /etc/gitlab-runner/config.toml")
              .then(res => res.stdout)
              .then(c => runnerConfig = c)
          })
          .catch(this.out.startBypass("fetch-config"))
          .then(this.out._it("isn't empty", () => runnerConfig.trim().length.should.be.above(0)))
          .catch(this.out.allowAssertion)
          .then(this.out._it("contains 'metrics server'", () => runnerConfig.should.contain("metrics_server = \":9999\"")))
          .catch(this.out.allowAssertion)
          .then(this.out._it(`contains ${prefix}`, () => runnerConfig.should.contain(prefix)))
          .catch(this.out.allowAssertion)
          .then(this.out._it(`includes instance ID`, () => runnerConfig.should.contain(runnerID)))
          .catch(this.out.allowAssertion)
          .catch(this.out.resumeBypass("fetch-config"));
      }))
      .then(() => this.out.section("GitLab Runner", () => {
        const countRunners = () => this.machine
          .ssh("sudo gitlab-runner list 2>&1 | tail -n +2 -f - | wc -l")
          .then(res => res.stdout)
          .then(parseInt);
        let initialRunners;
        return sleep(30 * 1000)
          .then(countRunners)
          .then(runners => {
            initialRunners = runners;
            console.log(`Starting with ${runners} runners.`);
            return this.out.it("should have at least 1 runner", () => runners.should.be.at.least(1));
          })
          .catch(this.out.startBypass("no-runners"))
          .then(this.out._it("should be registered with GitLab", () => this.machine.ssh("sudo gitlab-runner verify")))
          .catch(this.out.allowAssertion)
          .catch(this.out.resumeBypass("no-runners"))
          .then(() => this.machine.ssh("sudo systemctl start unregister-runners"))
          .then(() => sleep(10 * 1000))
          .then(() => countRunners())
          .then(runners => this.out.it("should unregister runners", () => runners.should.equal(0)))
          .catch(this.out.allowAssertion)
          .then(() => this.machine.ssh("sudo systemctl start register-runners"))
          .then(() => sleep(10 * 1000))
          .then(() => countRunners())
          .then(runners => this.out.it("should re-register runners", () => runners.should.equal(initialRunners)))
          .catch(this.out.allowAssertion);
      }))
      .then(() => this.out.section("Metrics Server", () => {
        function query(domain) { return `curl http://${domain}:9999/metrics >/dev/null`; }
        return this.out
          .it("should be reachable via 'localhost'", () => this.machine.ssh(query("localhost")))
          .catch(this.out.startBypass("no-metrics-server"))
          .then(this.out._it("should be reachable inside 'stackdriver-relay'", () =>
            this.machine.ssh(`sudo docker exec stackdriver-relay /bin/sh -c 'apk add --no-cache curl; ${query("runner")}' >/dev/null`)
          ))
          .catch(this.out.allowAssertion)
          .catch(this.out.resumeBypass("no-metrics-server"));
      }))
      .catch(err => {
        console.error(chalk.red("[ERROR]") + " Testing encountered unhandled error:");
        console.error(err);
        throw err;
      });
  }

  cleanup() {
    return this
      .stopRunners()
      .then(() => super.cleanup());
  }

  /**
   * Unregisters the runners from the GitLab server.
  */
  stopRunners() {
    const wait = 30;
    this.io.status("Stopping runners.");
    return this.machine
      .ssh(`sudo systemctl start unregister-runners`)
      .then(() => console.log(`Runner stop command sent.  Waiting ${wait}s to ensure GitLab is informed.`))
      .then(() => sleep(wait * 1000))
      .catch(err => {
        console.error(chalk.red("Unable to stop GitLab runners."));
        console.log(err)
        this.exitCode = 1;
      });
  }

  /**
   * If old `cloud-init` files saved to disk still exist, ask the user if they should be cleaned up.
   * @return {Promise} resolves when log files have been cleaned, if needed.
   * @todo Implement method.
  */
  cleanOldLogs() {
    return Promise.resolve();
  }

  /**
   * Wait for the `cloud-init` process to report that it's finished.
   * @return {Promise} resolves when `cloud-init` reports being finished.
  */
  waitForCloudInit() {
    this.io.status("Waiting for 'cloud-init' to finish.");
    const finished = "/var/lib/cloud/instance/boot-finished";
    return this.machine.ssh(`while [ ! -f '${finished}' ]; do sleep 1; done`);
  }

  /**
   * Retrieves the `cloud-init` logfile, and optionally prints to the console and writes out to a file.
   * @return {Promise} resolves when `cloud-init` log has been dealt with.
  */
  fetchCloudInitLog() {
    let cloudInitLog;
    return this.machine
      .ssh(`cat /var/log/cloud-init-output.log`)
      .then(res => res.stdout)
      .then(l => cloudInitLog = l)
      .then(() => this.io.yesNo("Print 'cloud-init' log output to console?", "Y", "Y"))
      .then(answer => console.log(answer ? cloudInitLog : "'cloud-init' log output supressed."))
      .then(() => this.io.yesNo("Save 'cloud-init' log to file?", "N", "Y"))
      .then(answer => {
        const outFile = `cloud-init-output-${this.machine.salt}.log`;
        if(answer) {
          return fs
            .writeFile(path.join(__dirname, outFile), cloudInitLog)
            .then(() => console.log(`Wrote log to ${outFile}.`));
        }
      });
  }

}

module.exports = RunnerConfigTest;

if(require.main === module) {
  test = new RunnerConfigTest();
  test.test();
}
