#!/bin/sh

TEMPLATE="scaling-ci-runner"
GROUP="rweda-ci-runners"
INSTANCE_PREFIX="ci-runner" # creates "ci-runner-yahs", "ci-runner-qtyz", etc.
ZONE="us-east1-d"
DESCRIPTION="An autoscaling group."
PRODUCTION="true"
METRIC="custom.googleapis.com/gitlab-ci-runner/builds"
TYPE="g1-small" # or 'n1-standard-1'
SIZE="0" # initial size of instance group
SCALE_TARGET="8" # target number of CI jobs
MAX_REPLICAS="3" # max number of machines to create
COOLDOWN="120" # seconds to wait after creating a new instance

template_exists() {
  gcloud compute instance-templates describe "$TEMPLATE" >/dev/null 2>&1
}

group_exists() {
  gcloud compute instance-groups describe "$GROUP" --zone "$ZONE" >/dev/null 2>&1
}

if template_exists; then
  echo "instance-template '$TEMPLATE' already exists.  Skipping."
else
  echo "Creating instance-template '$TEMPLATE'"
  # See https://cloud.google.com/sdk/gcloud/reference/compute/instance-templates/create
  gcloud compute instance-templates create "$TEMPLATE" \
  --description "$DESCRIPTION" \
  --machine-type "$TYPE" \
  --image-family ubuntu-1604-lts \
  --image-project ubuntu-os-cloud \
  --preemptible \
  --metadata production="$PRODUCTION" \
  --metadata-from-file user-data=cloud-config.yaml || {
    echo "Failed to create instance-template.  Aborting."
    exit 1
  }
  echo "Created instance template '$TEMPLATE'."
fi

if group_exists; then
  echo "instance-group '$GROUP' already exists.  Skipping."
else
  echo "Creating instance-group '$GROUP'"
  # See https://cloud.google.com/compute/docs/instance-groups/creating-groups-of-managed-instances
  gcloud compute instance-groups managed create "$GROUP" \
    --base-instance-name "$INSTANCE_PREFIX" \
    --size "$SIZE" \
    --template "$TEMPLATE" \
    --zone "$ZONE" || {
      echo "Failed to create instance-group.  Aborting."
      exit 1
    }
  echo "Created instance group '$GROUP' with '$SIZE' machines."
fi

echo "Setting autoscaling for instance-group"

gcloud compute instance-groups managed set-autoscaling "$GROUP" \
  --zone "$ZONE" \
  --custom-metric-utilization "metric=${METRIC},utilization-target-type=GAUGE,utilization-target=${SCALE_TARGET}" \
  --max-num-replicas "$MAX_REPLICAS" \
  --cool-down-period "$COOLDOWN"
