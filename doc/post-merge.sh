#!/bin/sh
rm config.toml || true
cp config.toml.template config.toml
rm /etc/gitlab-runner/config.toml || true
cp config.toml /etc/gitlab-runner/config.toml
systemctl restart gitlab-runner
