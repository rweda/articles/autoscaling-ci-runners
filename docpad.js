module.exports = {

  collections: {

    index: function() {
      return this
        .getCollection("html")
        .findAllLive({ outFilename: "index.html" })
        .on("add", (model) => model.setMetaDefaults({ layout: "article" }));
    },

    noRender: function() {
      return this
        .getCollection("documents")
        .findAllLive({ "$not": { outFilename: "index.html" } })
        .on("add", (model) => model.setMetaDefaults({
          render: false,
          outFilename: model.attributes.filename,
          contentRendered: model.attributes.body,
          rendered: true,
        }));
    },

  },

};
